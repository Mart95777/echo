<!DOCTYPE html>
<html>
<head>
<?php
$servername = "localhost";
$username = "root";
$password = "pass";
$dbname = "echodb1";
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
};
// mapTypeId: google.maps.MapTypeId.ROADMAP
// google map
?>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBzurpFi2vVXcr79Z06KCRJ4OvPbQM0BXM"></script>
<script>
function initmap() {
    var myOptions = {
      zoom: 10,
      center:new google.maps.LatLng(54.30, 18.80)
    };
    var map = new google.maps.Map(
        document.getElementById("map_canvas1"),
        myOptions);
  }
 google.maps.event.addDomListener(window, 'load', initmap);
</script>
</head>
<body>
	<?php
if ($result = $conn->query("SELECT lp,tytul,opis,lati,longi FROM ciekawostka")) {
    while($row = $result->fetch_assoc()) {
        echo "lp: " . $row["lp"]. " - tytul: " . $row["tytul"]
        . "<br>". "Opis: " . $row["opis"]. "<br>";
    }
} else {
    echo "0 results";
}

$conn->close();
?> 
<div id="map_wrap" style="margin:0; padding:0; hight:100%; width:100%;"> 
	<div id="map_canvas1" style="width: 500px; height: 400px; background-color: gray"></div>
</div>
</body>
</html>
